package exercise;

class App {

    public static int[] reverse(int[] numbers) {

        for (int i = 0; i < numbers.length / 2; i++) {
            int temp = numbers[numbers.length - i - 1];
            numbers[numbers.length - i - 1] = numbers[i];
            numbers[i] = temp;
        }

        //Collections.reverse(Arrays.asList(numbers));
        return numbers;
    }

    public static int getIndexOfMaxNegative(int[] numbers) {
        int ans = -1;
        int maxNegative = Integer.MIN_VALUE;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] < 0 && maxNegative < numbers[i]) {
                maxNegative = numbers[i];
                ans = i;
            }
        }

        return ans;
    }
}
