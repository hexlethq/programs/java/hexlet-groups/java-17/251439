package exercise;

class App {
    public static void numbers() {
        // BEGIN
        final int x = 8 / 2;
        final int y = 100 % 3;

        System.out.println(x + y);
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        System.out.println(language + " works on JVM");
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        StringBuilder ans = new StringBuilder();
        ans.append(soldiersCount)
        .append(" ")
        .append(name);

        System.out.println(ans);
        // END
    }
}
