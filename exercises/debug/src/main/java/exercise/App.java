package exercise;

import java.util.Arrays;

class App {

    public static final String NOT_A_TRIANGLE = "Треугольник не существует";
    public static final String EQUILATERAL = "Равносторонний";
    public static final String ISOSCELES = "Равнобедренный";
    public static final String VERSATILE = "Разносторонний";

    public static String getTypeOfTriangle(final int a, final int b, final int c) {

        int[] triangleSides = {a, b, c};
        Arrays.sort(triangleSides);

        if (triangleSides[0] < 1 || triangleSides[2] > triangleSides[0] + triangleSides[1]) {
            return NOT_A_TRIANGLE;
        }
        if (triangleSides[0] == triangleSides[1] && triangleSides[1] == triangleSides[2]) {
            return EQUILATERAL;
        }
        if (triangleSides[0] == triangleSides[1] || triangleSides[1] == triangleSides[2]) {
            return ISOSCELES;
        }

        return VERSATILE;
    }
}
