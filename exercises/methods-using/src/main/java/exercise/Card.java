package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        StringBuilder result = new StringBuilder();

        result.append("*".repeat(Math.max(0, starsCount)));

        for (int i = cardNumber.length() - 4; i < cardNumber.length(); i++) {
            result.append(cardNumber.charAt(i));
        }

        System.out.println(result);
        // END
    }
}
