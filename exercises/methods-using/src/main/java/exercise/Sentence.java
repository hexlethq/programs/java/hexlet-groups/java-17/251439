package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        if (sentence.charAt(sentence.length() - 1) == '!') {
            sentence = sentence.toUpperCase();
        } else {
            sentence = sentence.toLowerCase();
        }

        System.out.println(sentence);
        // END
    }
}
