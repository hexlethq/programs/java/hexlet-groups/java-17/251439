package exercise;

class Converter {

    public static int convert(final int n, final String convertTo) {

        if ("b".equals(convertTo)) {
            return n * 1024;
        } else if ("Kb".equals(convertTo)) {
            return n / 1024;
        } else {
            return 0;
        }
    }

    public static void main(String[] args) {

        System.out.printf("10 Kb = %d b\n", Converter.convert(10, "b"));
    }
}
