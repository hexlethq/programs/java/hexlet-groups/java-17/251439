package exercise;

class Triangle {

    public static double getSquare(int a, int b, int angle) {

        double radian = Math.PI * angle / 180;

        return Math.sin(radian) * a * b / 2;
    }

    public static void main(String[] args) {

        System.out.println(Triangle.getSquare(4, 5, 45));
    }
}
