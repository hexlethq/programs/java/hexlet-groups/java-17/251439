package exercise;

class App {

    public static String getAbbreviation(String text) {
        String[] words = text.trim().split(" ");
        StringBuilder abbreviation = new StringBuilder();

        for (String word : words) {
            abbreviation.append(word.charAt(0));
        }

        return abbreviation.toString().toUpperCase();
    }
}
